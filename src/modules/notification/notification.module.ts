import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/modules/database/database.module';
import { NotificationProvider } from '../notification/notification.provider';
import { NotificationService } from '../notification/notification.service';
import { NotificationController } from 'src/modules/notification/notification.controller';
import { UserProvider } from '../user/user.provider';

@Module({
  imports: [DatabaseModule],
  providers: [...NotificationProvider, ...UserProvider, NotificationService],
  controllers: [NotificationController],
})
export class NotificationModule {}
