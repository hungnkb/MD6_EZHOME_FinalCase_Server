import { Module } from '@nestjs/common';
import { PaypalController } from '../paypal/paypal.controller';
import { PaypalService } from '../paypal/paypal.service';

@Module({
  controllers: [PaypalController],
  providers: [PaypalService],
})
export class PaypalModule {}
