import { Module } from '@nestjs/common/decorators';
import { DatabaseModule } from '../database/database.module';
import { databaseProviders } from '../database/database.providers';
import { ReviewController } from './review.controller';
import { ReviewProvider } from './review.provider';
import { ReviewService } from './review.service';

@Module({
  imports: [DatabaseModule],
  controllers: [ReviewController],
  providers: [...databaseProviders, ...ReviewProvider, ReviewService],
})
export class ReviewModule {}
