import { DataSource } from 'typeorm';
import { OrderSchema } from '../order/order.entity';

export const OrderProvider = [
  {
    provide: 'ORDER_REPOSITORY',
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(OrderSchema),
    inject: ['DATA_SOURCE'],
  },
];
